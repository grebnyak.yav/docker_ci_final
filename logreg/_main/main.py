import numpy as np


# Multiclass logistic regression implementation

class LogisticRegression:
    """
    Logistic regression classifier.

    Data should be normalized.

    -------------------

    Parameters

    -------------------

    penalty: {l2', 'none'}, default='l2'

        Specify the norm of the penalty:

            - 'none': no penalty is added;

            - 'l2': add a L2 penalty term and it is the default choice;

    tol: float, default=1e-4
        Tolerance for stopping criteria.

    C: float, default=1.0
        Inverse of regularization strength; must be a positive float.
        Smaller values specify stronger regularization.

    fit_intercept: bool, default=True
        Specifies if a constant should be added to the decision function.

    max_iter: int, default=1000
        Maximum number of iterations taken for the solvers to converge.

    lr: float, default=0.1
        Learning rate.

    -------------------

    Methods:

    -------------------

    fit(X, y)
        Fit the model according to the given training data.

    predict(X)
        Predict class labels for samples in X.

    -------------------

    Attributes:

    -------------------

    classes_: ndarray of shape (n_classes, )
        A list of class labels known to the classifier.

    coef_: ndarray of shape (n_classes, n_features)
        Coefficient of the features in the decision function.

    intercept_: ndarray of shape (n_classes,)
        Intercept added to the decision function.

    """
    classes_ = None
    coef_ = None
    intercept_ = 0.0

    def __init__(self, *args, **kwargs):
        default_dict = {
            "penalty": "l2",
            "tol": 1e-4,
            "C": 1.0,
            "fit_intercept": True,
            "max_iter": 1000,
            "lr": 0.1
        }

        for key in kwargs:
            if key not in default_dict:
                raise ValueError(f"Key '{key}' not allowed")
            default_dict[key] = kwargs[key]

        if default_dict["penalty"] not in {'l2', 'none'}:
            raise ValueError("penalty should be in {'l2', 'none'}")

        self._penalty = default_dict["penalty"]

        self._tol = float(default_dict["tol"])

        if self._tol <= 0:
            raise ValueError("tol should be greater than 0")

        self._C = float(default_dict["C"])

        if self._C <= 0:
            raise ValueError("C should be greater than 0")

        self._fit_intercept = default_dict["fit_intercept"]

        self._max_iter = int(default_dict["max_iter"])

        if self._max_iter <= 0:
            raise ValueError("max_iter should be greater than 0")

        self._lr = float(default_dict["lr"])

        if self._lr <= 0:
            raise ValueError("lr should be greater than 0")

        self._kwargs = kwargs

    def fit(self, X, y):
        """
        Fit the model according to the given training data.

        -------------------

        Parameters

        -------------------

        X: array-like of shape (n_samples, n_features)
            Training vector, where n_samples is the number of samples
            and n_features is the number of features.

        y: array-like of shape (n_samples,)
            Target vector relative to X.

        -------------------

        Returns

        -------------------

        self
            Fitted estimator.

        """
        X_matrix = np.matrix(X)

        if self._fit_intercept:
            X = np.ones((X_matrix.shape[0], X_matrix.shape[1] + 1))
            X[:, :-1] = X_matrix
        else:
            X = X_matrix

        self.classes_ = list(set(y))

        classes_dict = {item: ind for ind, item in enumerate(self.classes_)}

        y = [classes_dict[item] for item in y]

        y_matrix = np.zeros((len(y), len(self.classes_)))

        for i in range(len(y)):
            y_matrix[i][y[i]] = 1

        y = np.matrix(y_matrix)

        self._solver(X, y)

        if self._fit_intercept:
            self.intercept_ = self.coef_[-1, :]
            self.coef_ = self.coef_[:-1]
        else:
            self.intercept_ = np.zeros(len(self.classes_))

        return self

    def _solver(self, X, y):
        if self._penalty == 'none' or self._penalty == 'l2':
            self._gd(X, y)

    def _gd(self, X, y):
        w = np.matrix(np.zeros((X.shape[1], len(self.classes_))))

        for _ in range(self._max_iter):
            exp_min_wX = np.exp(-np.dot(X, w))

            probas = exp_min_wX / exp_min_wX.sum(axis=1)

            grad_0 = (np.dot(X.T, y) - np.dot(X.T, probas)) / len(y)
            grad_1 = self._grad(w)

            grad = grad_1 / self._C + grad_0

            w = w - grad * self._lr

            if np.all(abs(grad) < self._tol):
                self._w = w
                self.coef_ = np.array(w)
                return

        self._w = w
        self.coef_ = np.array(w)
        print('The maximum number of iterations has been reached. Tolerance is {}'.format(np.max(abs(grad))))

    def _grad(self, w):
        if self._penalty == "l2":
            return w
        elif self._penalty == "none":
            return 0

    def predict(self, X):
        """
        Predict class labels for samples in X.

        -------------------

        Parameters

        -------------------

        X: array-like of shape (n_samples, n_features)
            Samples.

        -------------------

        Returns

        -------------------

        C: array of shape [n_samples]
            Predicted class label per sample.

        """
        X_matrix = np.matrix(X)

        if self._fit_intercept:
            X = np.ones((X_matrix.shape[0], X_matrix.shape[1] + 1))
            X[:, :-1] = X_matrix
        else:
            X = X_matrix

        exp_min_Xw = np.exp(-np.dot(X, self._w))

        probas = np.array(exp_min_Xw / exp_min_Xw.sum(axis=1))

        result = probas.argmax(axis=1).T

        return np.array([self.classes_[ind] for ind in result])

    def __repr__(self):
        return "LogisticRegression({})".format(', '.join(map(lambda x:
                                                              str(x) + \
                                                              ' = ' + \
                                                              str(self._kwargs[x]), self._kwargs)))