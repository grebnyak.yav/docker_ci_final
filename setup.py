import os
import logreg
from setuptools import setup, find_packages


def read_description():
    abs_path = os.path.join(os.path.dirname(__file__), "README.md")
    with open(abs_path) as f:
        return f.read()


setup(
    name="logreg",
    version=logreg.__version__,
    packages=find_packages(),
    install_requires=["numpy>=1.19.5"],
    description="Multiclass logistic regression classifier",
    long_description=read_description(),
    python_requires=">=3.6"
)