import numpy as np
from logreg import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.datasets import make_blobs, load_iris, load_breast_cancer


def main():
    # Check on two separate classes
    X, y = make_blobs(n_samples=100,
                      n_features=2,
                      centers=[(-10, -10), (10, 10)],
                      cluster_std=1.,
                      random_state=42)
    classifier = LogisticRegression()
    classifier.fit(X, y)
    res = classifier.predict([[-10, -10], [10, 10]])

    if res[0] != 0 or res[1] != 1:
        raise RuntimeError("Wrong predictions on centers, separate two clusters")

    res = classifier.predict(X)

    if not np.all(y == res):
        raise RuntimeError("Wrong predictions on data, separate two clusters")

    # Check on two close classes
    X, y = make_blobs(n_samples=100,
                      n_features=2,
                      centers=[(-1, -1), (1, 1)],
                      cluster_std=1.,
                      random_state=42)
    classifier = LogisticRegression()
    classifier.fit(X, y)
    res = classifier.predict([[-1, -1], [1, 1]])

    if res[0] != 0 or res[1] != 1:
        raise RuntimeError("Wrong predictions on centers, close two clusters")

    res = classifier.predict(X)

    if accuracy_score(res, y) < 0.8:
        raise RuntimeError("Wrong predictions on data, close two clusters")

    # Check three separate classes
    X, y = make_blobs(n_samples=100,
                      n_features=3,
                      centers=[(-10, -10), (10, 10), (-10, 10)],
                      cluster_std=1.,
                      random_state=42)
    classifier = LogisticRegression()
    classifier.fit(X, y)
    res = classifier.predict([[-10, -10], [10, 10], [-10, 10]])

    if res[0] != 0 or res[1] != 1 or res[2] != 2:
        raise RuntimeError("Wrong predictions on centers, separate three clusters")

    res = classifier.predict(X)

    if not np.all(y == res):
        raise RuntimeError("Wrong predictions on data, separate three clusters")

    # Check on three close classes
    X, y = make_blobs(n_samples=100,
                      n_features=3,
                      centers=[(-1.5, -1.5), (1.5, 1.5), (-1.5, 1.5)],
                      cluster_std=1.,
                      random_state=42)
    classifier = LogisticRegression()
    classifier.fit(X, y)
    res = classifier.predict([[-1.5, -1.5], [1.5, 1.5], [-1.5, 1.5]])

    if res[0] != 0 or res[1] != 1 or res[2] != 2:
        raise RuntimeError("Wrong predictions on centers, close three clusters")

    res = classifier.predict(X)

    if accuracy_score(res, y) < 0.8:
        raise RuntimeError("Wrong predictions on data, close three clusters")

    # Check on iris dataset
    X = StandardScaler().fit_transform(load_iris().data)
    y = load_iris().target
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=42)

    classifier = LogisticRegression(max_iter=10000, penalty="l2", C = 1, lr=0.1, tol=1e-6)
    classifier.fit(X_train, y_train)

    if accuracy_score(classifier.predict(X_test), y_test) < 0.7:
        raise RuntimeError("Wrong prediction on iris dataset, l2 penalty")

    # Check on breast cancer
    X = StandardScaler().fit_transform(load_breast_cancer().data)
    y = load_breast_cancer().target
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=42)

    classifier = LogisticRegression(max_iter=100000, penalty="none", lr=1)
    classifier.fit(X_train, y_train)

    if accuracy_score(classifier.predict(X_test), y_test) < 0.8:
        raise RuntimeError("Wrong prediction on breast cancer dataset, no penalty")


if __name__ == '__main__':
    main()
