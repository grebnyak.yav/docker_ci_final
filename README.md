Description
===========

Multiclass logistic regression classifier

Data should be normalized.

-------------------

Parameters

-------------------

penalty: {l2', 'none'}, default='l2'

    Specify the norm of the penalty:

        - 'none': no penalty is added;

        - 'l2': add a L2 penalty term and it is the default choice;

tol: float, default=1e-4

    Tolerance for stopping criteria.

C: float, default=1.0

    Inverse of regularization strength; must be a positive float.
    Smaller values specify stronger regularization.

fit_intercept: bool, default=True

    Specifies if a constant should be added to the decision function.

max_iter: int, default=1000

    Maximum number of iterations taken for the solvers to converge.

lr: float, default=0.1

    Learning rate.

-------------------

Methods:

-------------------

fit(X, y)

    Fit the model according to the given training data.

predict(X)

    Predict class labels for samples in X.

-------------------

Attributes:

-------------------

classes_: ndarray of shape (n_classes, )

    A list of class labels known to the classifier.

coef_: ndarray of shape (n_classes, n_features)

    Coefficient of the features in the decision function.

intercept_: ndarray of shape (n_classes,)

    Intercept added to the decision function.

