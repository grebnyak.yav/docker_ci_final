import numpy as np
from logreg import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.datasets import make_blobs, load_iris, load_breast_cancer


if __name__ == '__main__':
    raise RuntimeError("Smth went wrong...")
